<?php

//set constant
define('BASE_URL',plugin_dir_path( __FILE__ ));
define('PLUGIN_URL', plugins_url() .'/event-listing/');
define('INCLUDE_DIR', plugin_dir_path( __FILE__ ) .'includes/');

//load action
include INCLUDE_DIR.'action/init-action.php';

//load shourtcodes add commit
include INCLUDE_DIR.'shortcodes/event-list.php';

?>