<div class="row">
	<?php foreach ($events as $index => $e) { ?>
		<?php $e_meta = get_post_meta($e->ID); ?>
		<div class="col-md-12 event-panel">
			<div class="panel panel-default">
		  		<div class="panel-heading el-event-con-head">
		  			<a href="<?= $e_meta['_event_url'][0] ?>" target="_blank" ><?= $e->post_title ?></a>
		  			<a href="http://www.google.com/calendar/event?
					action=TEMPLATE
					&text=<?= str_replace(' ', '+',$e->post_title) ?>
					&dates=<?= date('Ymd',$e_meta['_date'][0]).'T120000Z/'.date('Ymd',$e_meta['_date'][0]).'T120000Z'; ?>
					&details=
					&location=<?= str_replace(' ', '+', $e_meta['_location'][0]) ?>
					&trp=false
					&sprop=
					&sprop=name:" target="_blank" class="el-gcalendar" >+G Calender</a>
		  		</div>
		  		<div class="panel-body el-event-con-body">
		  			<div class="desc">
		  			<p class="el-location" ><?= $e_meta['_location'][0] ?></p>
		  			<p class="el-date" ><?= date('F d, Y',$e_meta['_date'][0]); ?></p>
		  			</div>
		  			<img  class="el-img"  src="https://maps.googleapis.com/maps/api/staticmap?center=<?= str_replace(' ', '+', $e_meta['_location'][0]) ?>&zoom=13&size=600x300&maptype=roadmap&key=AIzaSyBlAL54ctT1Reg9MRIuUJ8Ekg8tbJ9HKb8" >
		  		</div>
			</div>
		</div>
	<?php } ?>
</div>