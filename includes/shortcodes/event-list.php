<?php

function event_list_init()
{
	$events = get_posts( array(
	    'post_type' => 'event_list',
	    'post_status' => "publish",
	    'order' => 'DESC',
	    'meta_key' => '_date',
        'orderby'   => '_date',
        'meta_query' => array(
            array(
            	'key' => '_date',
            )
        )
	) );
	
	include "views/list.php";
	//wp_enqueue_style('event-list-css', PLUGIN_URL .'assets/css/bootstrap.min.css');
	wp_enqueue_style('event-list-css', PLUGIN_URL .'assets/css/event-list.css');
	wp_enqueue_script('event-list-js', PLUGIN_URL .'assets/js/event-list.js', array('jquery'), null, true);
}

add_shortcode('event_list', 'event_list_init');
?>