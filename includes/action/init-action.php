<?php

//add admin page for event listing
function event_post_type() {
  register_post_type( 'event_list',
    array(
      'labels' => array(
        'name' => __( 'Event Lists' ),
        'singular_name' => __( 'Event list' )
      ),
      'public' => true,
      'has_archive' => true,
    )
  );
}
add_action( 'init', 'event_post_type' );

function add_events_metaboxes() {
	add_meta_box('el_info', 'Event Info', 'el_events_info', 'event_list', 'normal', 'default');
	//add_meta_box('el_events_location', 'Event Location', 'el_events_location', 'event_list', 'normal', 'default');
}
add_action( 'add_meta_boxes', 'add_events_metaboxes' );


function el_events_info() {
	global $post;
	
	echo '<input type="hidden" name="el-token" id="el-token" value="' . 
	wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
	
	$date = get_post_meta($post->ID, '_date', true);
	if($date != "")
		$date = date('F d, Y',$date);
	
	$location = get_post_meta($post->ID, '_location', true);
	$url = get_post_meta($post->ID, '_event_url', true);
	
	echo "<label>Date</label><br/>";
	echo '<input type="text" name="_date" id="_date" value="' . $date  . '" class="" /><br/>';
	echo "<label>Location</label><br/>";
	echo '<input type="text" name="_location" id="_location" value="' . $location  . '" class="" /><br/>';
	echo "<label>Url</label><br/>";
	echo '<input type="url" name="_event_url" id="_event_url" value="' . $url  . '" class="" /><br/>';

}


function load_event_list_style() {
	wp_enqueue_style( 'admin_css_event_list', PLUGIN_URL . 'assets/css/admin-event-list.css', false, '1.0.0' );
	wp_enqueue_style( 'admin_css_event_list_jquery_ui', PLUGIN_URL . 'assets/css/jquery-ui.min.css', false, '1.0.0' );
}  
add_action( 'admin_enqueue_scripts', 'load_event_list_style' );

function el_admin_js( $hook ) {
	if ( 'post.php' === $hook || 'post-new.php' === $hook ) {
		wp_enqueue_script( 'el-admin-js', PLUGIN_URL . 'assets/js/admin-event-list.js',  array( 'jquery', 'jquery-ui-datepicker', 'jquery-ui-slider' ) );
	}
}

add_action( 'admin_enqueue_scripts', 'el_admin_js' );


function el_save_event($post_id, $post) {
	
	if ( !wp_verify_nonce( $_POST['el-token'], plugin_basename(__FILE__) )) {
	return $post->ID;
	}

	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;

	
	$events_meta['_date'] = strtotime($_POST['_date']);
	$events_meta['_location'] = $_POST['_location'];
	$events_meta['_event_url'] = $_POST['_event_url'];
	
	foreach ($events_meta as $key => $value) { 
		if( $post->post_type == 'revision' ) return;
		$value = implode(',', (array)$value); 
		if(get_post_meta($post->ID, $key, FALSE)) {
			update_post_meta($post->ID, $key, $value);
		} else { 
			add_post_meta($post->ID, $key, $value);
		}
		if(!$value) delete_post_meta($post->ID, $key);
	}

}

add_action('save_post', 'el_save_event', 1, 2);

?>