<?php
/**
* Plugin Name: Event Lising
* Description: simple event listing
* Version: 1.0
*/

//register
function event_list_activate() {
    
    $event_listing_page = array(
      'post_title'    => 'Event Listing',
      'post_content'  => '[event_list]',
      'post_status'   => 'publish',
      'post_author'   => get_current_user_id(),
      'post_type'     => 'page',
    );

    wp_insert_post( $event_listing_page, '' );
}
register_activation_hook( __FILE__, 'event_list_activate' );


include 'index.php';

?>